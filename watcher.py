import time
import contextlib

import os
import datetime
import watchdog.events
import watchdog.observers

import json

class PausingObserver(watchdog.observers.Observer):
    def dispatch_events(self, *args, **kwargs):
        if not getattr(self, '_is_paused', False):
            super(PausingObserver, self).dispatch_events(*args, **kwargs)

    def pause(self):
        self._is_paused = True

    def resume(self):
        time.sleep(self.timeout)  # allow interim events to be queued
        self.event_queue.queue.clear()
        self._is_paused = False

    @contextlib.contextmanager
    def ignore_events(self):
        self.pause()
        yield
        self.resume()


class ChangesHandler(watchdog.events.FileSystemEventHandler):

    log_path = ""
    
    def log(self, str):
        with open(self.log_path, 'a') as f:
            f.write(datetime.datetime.now().strftime("%H:%M:%S") + ' ' + str)

    def on_modified(self, event):
        with OBSERVER.ignore_events():
            self.log("modified: " + event.src_path)
           
    def on_created(self, event):
        with OBSERVER.ignore_events():
            self.log("created: " + event.src_path)
            if (event.src_path == plan_file_path ):
                os.system(command.format(event.src_path, ip, port))

    def on_deleted(self, event):
        with OBSERVER.ignore_events():
            self.log("deleted: " + event.src_path)


config_path = '/home/const/.scheduler/config.json'

''' watcher config content: 
"watcher" : {
		"port": "",
		"watchdir": "/home/const/.scheduler/plan/",
		"plan_file_path": "/home/const/.scheduler/plan/plan.json",
		"ip": "localhost",
		"port": "8081",
		"command": "curl -X POST -H \"Content-Type: application/json\" -d @{0} {1}:{2}"
   }
'''

if __name__ == '__main__':
    
    config_data = {}
    with open(config_path) as f:
        config_data = json.load(f)["watcher"]

    port = config_data["port"]
    watchdir = config_data["watchdir"]
    plan_file_path = config_data["plan_file_path"]
    ip = config_data["ip"]
    command = config_data["command"]
    path_to_log = config_data["log_path"]
    
    handler = ChangesHandler()
    handler.log_path = path_to_log

    # if not os.path.exists(watchdir):
    #     os.makedirs(watchdir)

    OBSERVER = PausingObserver()
    OBSERVER.schedule(handler, watchdir, recursive=True)
    OBSERVER.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        OBSERVER.stop()
    OBSERVER.join()
